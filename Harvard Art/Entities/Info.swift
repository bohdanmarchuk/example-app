//
//  Info.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

struct Info: Codable{
    let totalrecordsperquery: Int
    let totalrecords: Int
    let pages: Int
    let page: Int
    let next: String
}
