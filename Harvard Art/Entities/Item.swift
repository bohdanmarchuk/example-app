//
//  Item.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

struct Item: Codable{
    let accessionyear: Int?
    let technique: String?
    let mediacount: Int?
    let totalpageviews: Int?
    let groupcount: Int?
    let colorcount: Int?
    let lastupdate: Date?
    let images: [Image]?
    let objectid: Int?
    let culture: String?
    let verificationleveldescription: String?
    let standardreferencenumber: String?
    let department: String?
    let state: String?
    let markscount: Int?
    let contact: String?
    let titlescount: Int?
    let id: Int?
    let title: String?
    let verificationlevel: Int?
    let division: String?
    let dated: String?
    let description: String?
    let style: String?
    let commentary: String?
    let relatedcount: Int?
    let labeltext: String?
    let totaluniquepageviews: Int?
    let dimensions: String?
    let exhibitioncount: Int?
    let techniqueid: Int?
    let dateend: Int?
    let creditline: String?
    let imagepermissionlevel: Int?
    let signed: String?
    let periodid: Int?
    let century: String?
    let classificationid: Int?
    let medium: String?
    let peoplecount: Int?
    let accesslevel: Int?
    let classification: String?
    let imagecount: Int?
}
