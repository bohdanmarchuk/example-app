//
//  Image.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 25/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

struct Image: Codable{
    let height: Int?
    let iiifbaseuri: String?
    let baseimageurl: String?
    let width: Int?
    let idsid: Int?
    let displayorder: Int?
    let copyright: String?
    let imageid: Int?
    let renditionnumber: String?
}
