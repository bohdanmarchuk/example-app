//
//  ListProtocols.swift
//  Harvard Art
//
//  Created Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

//MARK: - Wireframe -
protocol ListWireframeProtocol: class {
    static func createModule() -> UIViewController
}

//MARK: - View -
protocol ListViewProtocol: class {
    
    var presenter: ListPresenterProtocol?  { get set }
    
    /* Presenter -> ViewController */
    func showItemsList(with items: [Item])
    func showError()
    func showLoading()
}

//MARK: - Presenter -
protocol ListPresenterProtocol: class {
    var view: ListViewProtocol? { get set }
    var interactor: ListInteractorInputProtocol? { get set }
    var router: ListWireframeProtocol? { get set }
    
    func retrieveItemsList(_ text: String?)
}

//MARK: - Interactor -
protocol ListInteractorOutputProtocol: class {
    
    /* Interactor -> Presenter */
    func didRetrieveItems(_ items: [Item])
    func onError()
}

protocol ListInteractorInputProtocol: class {
    var presenter: ListInteractorOutputProtocol? { get set }
    var remoteDatamanager: ListRemoteDataManagerInputProtocol? { get set }
    
    /* Presenter -> Interactor */
    func retrieveItemsList(_ text: String?)
}

//MARK: - Data manager -
protocol ListRemoteDataManagerInputProtocol: class {
    var remoteRequestHandler: ListRemoteDataManagerOutputProtocol? { get set }
    
    /* Interactor -> Remote data manager */
    func retrieveItemsList(_ text: String?)
}

protocol ListRemoteDataManagerOutputProtocol: class {
    /* Remote data manager -> Interactor */
    func onItemsRetrieved(_ items: [Item])
    func onError()
}
