//
//  ListRouter.swift
//  Harvard Art
//
//  Created Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

class ListRouter: ListWireframeProtocol {
    
    static func createModule() -> UIViewController {
        let view = ListViewController()
        let interactor: ListInteractorInputProtocol & ListRemoteDataManagerOutputProtocol = ListInteractor()
        let remoteDataManager: ListRemoteDataManagerInputProtocol = ItemsListRemoteDataManager()
        let wireframe: ListWireframeProtocol = ListRouter()
        let presenter: ListPresenterProtocol & ListInteractorOutputProtocol = ListPresenter()
    
        view.presenter = presenter
        presenter.view = view
        presenter.router = wireframe
        presenter.interactor = interactor
        interactor.presenter = presenter
        interactor.remoteDatamanager = remoteDataManager
        remoteDataManager.remoteRequestHandler = interactor

        return UINavigationController(rootViewController: view)
    }
}
