//
//  ListInteractor.swift
//  Harvard Art
//
//  Created Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

class ListInteractor: ListInteractorInputProtocol {
    
    var presenter: ListInteractorOutputProtocol?
    var remoteDatamanager: ListRemoteDataManagerInputProtocol?
    
    func retrieveItemsList(_ text: String?) {
        remoteDatamanager?.retrieveItemsList(text)
    }
}

extension ListInteractor: ListRemoteDataManagerOutputProtocol {
    func onItemsRetrieved(_ items: [Item]) {
        presenter?.didRetrieveItems(items)
    }
    
    func onError() {
        presenter?.onError()
    }
    
}
