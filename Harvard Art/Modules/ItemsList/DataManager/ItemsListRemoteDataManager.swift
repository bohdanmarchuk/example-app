//
//  ItemsListRemoteDataManager.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 25/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

class ItemsListRemoteDataManager: ListRemoteDataManagerInputProtocol {

    private let client = MuseumItemsListClient()
    
    private var currentPage = 0
    private var pageSize = 15
    
    var remoteRequestHandler: ListRemoteDataManagerOutputProtocol?
    
    func retrieveItemsList(_ text: String? = nil) {
        client.getList(from: .fetchList(text)) { [weak self] result in
            guard let strongSelf = self else { return }
            switch result {
            case .success(let museumItems):
                guard let items = museumItems?.records else {
                    strongSelf.remoteRequestHandler?.onError()
                    return
                }
                strongSelf.remoteRequestHandler?.onItemsRetrieved(items)
            case .failure:
                strongSelf.remoteRequestHandler?.onError()
            }
        }
    }
}
