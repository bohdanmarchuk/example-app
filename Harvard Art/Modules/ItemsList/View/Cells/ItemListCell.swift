//
//  ItemListCell.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 25/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

class ItemListCell: UITableViewCell {
    @IBOutlet weak var itemImage: UIImageView?
    
    @IBOutlet weak var itemName: UICustomItemLabel?
    @IBOutlet weak var itemDimensions: UICustomItemLabel?
    
    @IBOutlet weak var itemDated: UICustomItemLabel?
    @IBOutlet weak var itemClassification: UICustomItemLabel?
    @IBOutlet weak var itemDivisions: UICustomItemLabel?
    @IBOutlet weak var itemCulture: UICustomItemLabel?
    
    func configure(with item: Item) {
        self.itemName?.text = item.title

        self.itemDated?.setup(value: item.dated, description: "Dated")
        self.itemCulture?.setup(value: item.culture, description: "Culture")
        self.itemDivisions?.setup(value: item.division, description: "Division")
        self.itemClassification?.setup(value: item.classification, description: "Classification")
        
        self.itemDimensions?.text = parseDimensions(item.dimensions)
        
        self.setupItemBaseImage(with: item.images)
        self.sizeToFit()
        
    }
    
    fileprivate func setupItemBaseImage(with images: [Image]?) {
        guard let localImages = images, localImages.count > 0 else {
            self.itemImage?.image = UIImage(named: "default_image")
            return
        }
        for image in localImages {
            guard let baseURL = image.baseimageurl else {
                continue
            }
            self.itemImage?.loadImageUsingCache(withUrl: baseURL)
            return
        }
    }
    
    func getDataFromUrl(url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            completion(data, response, error)
            }.resume()
    }
}

extension ItemListCell {
    fileprivate func parseDimensions(_ value: String?) -> String {
        guard var mutableValue = value else { return "" }
        mutableValue = mutableValue.replacingOccurrences(of: "image:", with: "")
        if let range = mutableValue.range(of: "(") {
            mutableValue.removeSubrange(range.lowerBound..<mutableValue.endIndex)
        }
        return mutableValue
    }
}

class UICustomItemLabel: UILabel {
    func setup(value: String?, description: String) {
        guard let _value = value else {
            self.isHidden = true
            return
        }
        
        let attributedDescription = NSAttributedString(string: "\(description): ", attributes: [ NSAttributedStringKey.font: UIFont(name: "Optima-Bold", size: 12) ?? UIFont.systemFont(ofSize: 12)])
        let attributedValue = NSAttributedString(string: _value, attributes: [ NSAttributedStringKey.font: UIFont(name: "Optima-Regular", size: 12) ?? UIFont.systemFont(ofSize: 12)])
        
        let generalAttributedValue = NSMutableAttributedString()
        generalAttributedValue.append(attributedDescription)
        generalAttributedValue.append(attributedValue)
        self.attributedText = generalAttributedValue
    }
}
