//
//  ListViewController.swift
//  Harvard Art
//
//  Created Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController, ListViewProtocol {

	var presenter: ListPresenterProtocol?
    fileprivate var itemsList: [Item] = [] {
        didSet {
            self.refreshControl?.endRefreshing()
            if itemsList.count == 0 {
                self.tableView.changeBackgroundView(to: .emptyResult)
            }
        }
    }

    fileprivate let searchController = UISearchController(searchResultsController: nil)
    fileprivate var backgroundView: ListBackgroundView?
    fileprivate var shownIndexes : [IndexPath] = []
    
	override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.configureSearchBar()
        self.presenter?.retrieveItemsList(nil)
        
        self.refreshControl = createRefreshControl()
    }
    
    @objc func refreshView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.presenter?.retrieveItemsList(nil)
        }
    }
}

//MARK: - ListViewProtocol -
extension ListViewController {
    func showItemsList(with items: [Item]) {
        self.itemsList = items
        self.tableView.reloadData()
    }
    
    func showError() {
        self.itemsList = []
        self.tableView.reloadData()
        self.tableView.changeBackgroundView(to: .emptyResult)
    }
    
    func showLoading() {
        self.itemsList = []
        self.tableView.reloadData()
        self.tableView.changeBackgroundView(to: .loading)
    }
}

//MARK: - UITableViewDataSource -
extension ListViewController {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        tableView.backgroundView?.isHidden = itemsList.count > 0 ? true : false
        return itemsList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ItemListCell", for: indexPath) as! ItemListCell
        cell.configure(with: self.itemsList[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if (shownIndexes.contains(indexPath) == false) {
            shownIndexes.append(indexPath)
            cell.animate()
        }
    }
}

//MARK: - UITableViewDelegate -
extension ListViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let images = self.itemsList[indexPath.row].images, images.count > 0 else {
            return
        }
    
        guard let photoPreview = UIStoryboard(name: "PhotoPreview", bundle: nil).instantiateInitialViewController() as? PhotoPreviewViewController else { return }
        photoPreview.title = self.itemsList[indexPath.row].title
        photoPreview.allPhotos = images
        self.present(photoPreview, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension ListViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text, !text.isEmpty else { return }
        self.presenter?.retrieveItemsList(text)
    }
}

extension ListViewController: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.presenter?.retrieveItemsList(nil)
    }
}

//MARK: - Private methods -
extension ListViewController {
    fileprivate func configureView(){
        self.registerCells()
        self.configureTitle()
        self.configureBackgroundView()
        self.configureTableView()
    }
    
    fileprivate func configureTableView(){
        self.tableView.tableFooterView = UIView()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 80
    }
    
    fileprivate func configureSearchBar(){
        self.searchController.isActive = false
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.definesPresentationContext = true
        self.searchController.searchBar.searchBarStyle = .minimal
        self.searchController.searchBar.delegate = self
        self.tableView.tableHeaderView = searchController.searchBar
        self.tableView.tableHeaderView?.backgroundColor = .white
        self.searchController.hidesNavigationBarDuringPresentation = false
    }
    
    fileprivate func createRefreshControl() -> UIRefreshControl {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshView), for: .valueChanged)
        return refreshControl
    }
    
    fileprivate func configureTitle(){
        self.title = "Museum"
        
        guard let font = UIFont(name: "Optima-Bold", size: 18) else {
            return
        }
        let attributesDictionary: [NSAttributedStringKey: Any]? = [
            NSAttributedStringKey.foregroundColor: UIColor.darkGray,
            NSAttributedStringKey.font: font
        ]
        self.navigationController?.navigationBar.titleTextAttributes = attributesDictionary
    }
    
    fileprivate func registerCells(){
        let nib = UINib(nibName: "ItemListCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "ItemListCell")
    }
    
    fileprivate func configureBackgroundView(){
        guard let view = UINib(nibName: "ListBackgroundView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? ListBackgroundView else {
            return
        }
        self.tableView.backgroundView = view
    }
}
