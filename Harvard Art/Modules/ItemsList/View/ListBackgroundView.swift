//
//  ListEmptyView.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 25/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

enum BackgroundViewStates: String {
    case loading = "Loading..."
    case searching = "Searching..."
    case emptyResult = "Nothing found"
    case connectionLost = "Connection lost"
    case error = "Something goes wrong :("
    
    var image: UIImage? {
        switch self {
        case .loading:
            return nil
        case .searching:
            return UIImage(named: "Searching")
        case .emptyResult:
            return UIImage(named: "Empty_search_result")
        case .connectionLost, .error:
            return UIImage(named: "Connection_lost")
        }
    }
}

class ListBackgroundView: UIView {
    @IBOutlet weak var backgroundImage: UIImageView?
    @IBOutlet weak var backgroundDescription: UILabel?
    
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView?
    
    var viewType: BackgroundViewStates? {
        didSet {
            self.backgroundImage?.image = viewType?.image
            self.backgroundDescription?.text = viewType?.rawValue
            
            if viewType == .loading {
                self.loadingIndicator?.isHidden = false
                self.loadingIndicator?.startAnimating()
            }else {
                self.loadingIndicator?.isHidden = true
                self.loadingIndicator?.stopAnimating()
            }
        }
    }
}
