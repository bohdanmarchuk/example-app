//
//  PhotoPreviewControllerViewController.swift
//  Harvard Art
//
//  Created by 4FreeD on 26/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

class PhotoPreviewViewController: UIViewController {
    
    //MARK: - @IBOutlets -
    @IBOutlet weak var controlsView: UIView!
    @IBOutlet weak var exitButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentWidthConstraint: NSLayoutConstraint!
    
    //MARK: - Public variables -
    var allPhotoScrollViews: Array<UIScrollView> = []
    var allPhotos: Array<Image> = []
    var statusBarHidden: Bool = false
    var scrollViewDragging: Bool = false
    
    //MARK: - Private variables -
    private var currentPhotoIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
        self.setupImageViews()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.contentWidthConstraint.constant = CGFloat(allPhotos.count) * scrollView.frame.size.width
        if !scrollViewDragging {
            self.scrollView.contentOffset = CGPoint(x: CGFloat(currentPhotoIndex) * scrollView.frame.size.width, y: 0)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.statusBarHidden = true
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.statusBarHidden = false
        self.setNeedsStatusBarAppearanceUpdate()
    }
    
    override var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
}

//MARK: - @IBActions -
extension PhotoPreviewViewController {
    @IBAction func backButtonSelected() {
        dismiss(animated: true, completion: nil)
    }
}

//MARK: - Private methods -
extension PhotoPreviewViewController {
    fileprivate func configureView(){
        self.view.backgroundColor = UIColor.black
        self.titleLabel.textColor = UIColor.white
        self.scrollView.delegate = self
        self.titleLabel.text = self.title
    }
    
    fileprivate func setupImageViews(){
        
        var previousView: UIView = contentView
        for x in 0...allPhotos.count-1 {
            
            let photo = allPhotos[x]
            
            let subScrollView = UIScrollView()
            subScrollView.delegate = self
            contentView.addSubview(subScrollView)
            allPhotoScrollViews.append(subScrollView)
            
            let imageView = UIImageView()
            imageView.loadImageUsingCache(withUrl: photo.baseimageurl!)
            imageView.contentMode = .scaleAspectFill
            subScrollView.addSubview(imageView)
            
            subScrollView.translatesAutoresizingMaskIntoConstraints = false
            let attribute: NSLayoutAttribute = (x == 0) ? .leading : .trailing
            scrollView.addConstraint(NSLayoutConstraint(item: subScrollView, attribute: .leading, relatedBy: .equal, toItem: previousView, attribute: attribute, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: subScrollView, attribute: .top, relatedBy: .equal, toItem: contentView, attribute: .top, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: subScrollView, attribute: .bottom, relatedBy: .equal, toItem: contentView, attribute: .bottom, multiplier: 1, constant: 0))
            view.addConstraint(NSLayoutConstraint(item: subScrollView, attribute: .width, relatedBy: .equal, toItem: scrollView, attribute: .width, multiplier: 1, constant: 0))
            
            imageView.translatesAutoresizingMaskIntoConstraints = false
            subScrollView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .width, relatedBy: .equal, toItem: imageView, attribute: .height, multiplier: 1, constant: 0))
            subScrollView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerX, relatedBy: .equal, toItem: subScrollView, attribute: .centerX, multiplier: 1, constant: 0))
            subScrollView.addConstraint(NSLayoutConstraint(item: imageView, attribute: .centerY, relatedBy: .equal, toItem: subScrollView, attribute: .centerY, multiplier: 1, constant: 0))
            
            for attribute: NSLayoutAttribute in [.top, .bottom, .leading, .trailing] {
                let constraintLowPriority = NSLayoutConstraint(item: imageView, attribute: attribute, relatedBy: .equal, toItem: subScrollView, attribute: attribute, multiplier: 1, constant: 0)
                let constraintGreaterThan = NSLayoutConstraint(item: imageView, attribute: attribute, relatedBy: .greaterThanOrEqual, toItem: subScrollView, attribute: attribute, multiplier: 1, constant: 0)
                constraintLowPriority.priority = UILayoutPriority(rawValue: 750)
                subScrollView.addConstraints([constraintLowPriority,constraintGreaterThan])
            }
            
            previousView = subScrollView
        }
        let xOffset = CGFloat(currentPhotoIndex) * scrollView.frame.size.width
        self.scrollView.contentOffset = CGPoint(x: xOffset, y: 0)
    }
    
    private func getCurrentPageIndex() -> Int {
        return Int(round(scrollView.contentOffset.x / scrollView.frame.size.width))
    }
}

//MARK: - UIScrollViewDelegate -
extension PhotoPreviewViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            scrollViewDragging = true
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView {
            scrollViewDragging = false
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.scrollView && scrollViewDragging {
            currentPhotoIndex = getCurrentPageIndex()
        }
    }
}
