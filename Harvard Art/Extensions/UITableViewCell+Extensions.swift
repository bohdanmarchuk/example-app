//
//  UITableViewCell+Extensions.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 27/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

extension UITableViewCell {
    func animate(with duration: Double = 0.5) {
        self.transform = CGAffineTransform(translationX: 0, y: 120)
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 10, height: 10)
        self.alpha = 0
        
        UIView.beginAnimations("rotation", context: nil)
        UIView.setAnimationDuration(duration)
        self.transform = CGAffineTransform(translationX: 0, y: 0)
        self.alpha = 1
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        UIView.commitAnimations()
    }
}
