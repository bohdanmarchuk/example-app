//
//  UITableView+Extensions.swift
//  Harvard Art
//
//  Created by 4FreeD on 26/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import UIKit

extension UITableView {
    func changeBackgroundView(to state: BackgroundViewStates) {
        if let view = self.backgroundView as? ListBackgroundView {
            view.isHidden = false
            view.viewType = state
        }else {
            assertionFailure("Didn't configured states background view")
        }
    }
}
