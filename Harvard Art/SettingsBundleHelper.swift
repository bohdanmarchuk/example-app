//
//  SettingsBundleHelper.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

class SettingsBundleHelper {
    class func setup() {
        self.setVersionAndBuildNumber()
    }
    
    private class func setVersionAndBuildNumber(){
        guard let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String else {
            return
        }
        guard let build = Bundle.main.object(forInfoDictionaryKey: "CFBundleVersion") as? String else {
            UserDefaults.standard.set(version, forKey: "version_preference")
            return
        }
        UserDefaults.standard.set("\(version) (\(build))", forKey: "version_preference")
    }
}
