//
//  ItemsListResult.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 25/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

struct ItemsListResult: Codable {
    let records: [Item]?
    let info: Info?
}
