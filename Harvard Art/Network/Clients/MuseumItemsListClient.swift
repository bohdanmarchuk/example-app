//
//  MuseumItemsListClient.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 25/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

class MuseumItemsListClient: APIClient {
    
    let session: URLSession
    
    init(configuration: URLSessionConfiguration) {
        self.session = URLSession(configuration: configuration)
    }
    
    convenience init() {
        self.init(configuration: .default)
    }
    
    func getList(from endpoint: ItemsFeed, completion: @escaping (Result<ItemsListResult?, APIError>) -> Void) {
        let request = endpoint.request
        
        fetch(with: request, decode: { json -> ItemsListResult? in
            guard let itemsListResult = json as? ItemsListResult else { return  nil }
            return itemsListResult
        }, completion: completion)
    }
}
