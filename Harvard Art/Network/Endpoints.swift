//
//  Endpoints.swift
//  Harvard Art
//
//  Created by Bohdan Marchuk on 23/02/2018.
//  Copyright © 2018 Bohdan Marchuk. All rights reserved.
//

import Foundation

protocol Endpoint {
    var base: String { get }
    var path: String { get }
    var parameters: [URLQueryItem]? { get }
}

extension Endpoint {
    
    var apiKey: String {
        let _apikey = "NTNkNmYzMzAtMThhMC0xMWU4LTg4MjQtZDFhYzRiMjRjOWY3"
        guard let decodedData = Data(base64Encoded: _apikey), let decodedString = String(data: decodedData, encoding: .utf8) else { return "" }
        return decodedString
    }
    
    var urlComponents: URLComponents {
        var components = URLComponents(string: base)!
        components.path = path
        components.queryItems = [
            URLQueryItem(name: "apikey", value: apiKey)
        ]
        
        if let items = parameters {
            for query in items {
                components.queryItems?.append(query)
            }
        }
        return components
    }
    
    var request: URLRequest {
        let url = urlComponents.url!
        return URLRequest(url: url)
    }
}

enum ItemsFeed {
    case fetchList(String?)
}

extension ItemsFeed: Endpoint {
    var base: String {
        return "https://api.harvardartmuseums.org"
    }
    
    var path: String {
        return "/object"
    }
    
    var parameters: [URLQueryItem]? {
        switch self {
        case .fetchList(let text):
            guard let searchText = text else { return nil }
            return [URLQueryItem(name: "title", value: searchText)]
        }
    }
}
